# MGDA
A linux desktop app for the #MindsGaming Community

[https://mgda.glitch.me](https://mgda.glitch.me)

---

Our community desktop app gives you quick access to our community website, Minds and more.  

## Install (Desktop App)

<code> wget https://mgda.glitch.me/MGDA.sh </code>

<code> sh ./MGDA.sh </code>

[View More Install Options](https://mgda.glitch.me/mgda/install)

---

## Install (Website)

You can just [Remix](https://glitch.com/edit/#!/remix/mgda) the project webiste assests on Glitch.

You can also grab the git <code>git clone https://3d208759-861d-44b0-9ef2-6453c65e96f8@api.glitch.com/git/mgda</code>

On the front-end

- Edit `views/index.html` to change the content of the webpage
- `public/client.js` is the javacript that runs when you load the webpage
- `public/style.css` is the styles for `views/index.html`
- Drag in `assets`, like images or music, to add them to your project

On the back-end,

- your app starts at `server.js`
- add frameworks and packages in `package.json`
- safely store app secrets in `.env` (nobody can see this but you and people you invite)

Click `Show` in the header to see your app live. Updates to your code will instantly deploy.


## Made by [Glitch](https://glitch.com/)

**Glitch** is the friendly community where you'll build the app of your dreams. Glitch lets you instantly create, remix, edit, and host an app, bot or site, and you can invite collaborators or helpers to simultaneously edit code with you.

Find out more [about Glitch](https://glitch.com/about).

( ᵔ ᴥ ᵔ )
