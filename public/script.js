// client-side js, loaded by index.html
// run by the browser each time the page is loaded

console.log("hello world :o");

function homeicon() {
  document.getElementById("homeicon").style.display = "block";
}

function closehomeicon() {
  document.getElementById("homeicon").style.display = "none";
}

function questionicon() {
  document.getElementById("questionicon").style.display = "block";
}

function closequestionicon() {
  document.getElementById("questionicon").style.display = "none";
}

function ogsmileicon() {
  document.getElementById("ogsmileicon").style.display = "block";
}

function closeogsmileicon() {
  document.getElementById("ogsmileicon").style.display = "none";
}

function closeicon() {
  document.getElementById("closeicon").style.display = "block";
}

function closecloseicon() {
  document.getElementById("closeicon").style.display = "none";
}

function coolsmileicon() {
  document.getElementById("coolsmileicon").style.display = "block";
}

function closecoolsmileicon() {
  document.getElementById("coolsmileicon").style.display = "none";
}

function loadingicon() {
  document.getElementById("loadingicon").style.display = "block";
}

function closeloadingicon() {
  document.getElementById("loadingicon").style.display = "none";
}

function sunicon() {
  document.getElementById("sunicon").style.display = "block";
}

function closesunicon() {
  document.getElementById("sunicon").style.display = "none";
}

function MDAicon() {
  document.getElementById("MDAicon").style.display = "block";
}

function closeMDAicon() {
  document.getElementById("MDAicon").style.display = "none";
}

function MindsGamingicon() {
  document.getElementById("MindsGamingicon").style.display = "block";
}

function closeMindsGamingicon() {
  document.getElementById("MindsGamingicon").style.display = "none";
}

function browsericon() {
  document.getElementById("browsericon").style.display = "block";
}

function closebrowsericon() {
  document.getElementById("browsericon").style.display = "none";
}

function bulbicon() {
  document.getElementById("bulbicon").style.display = "block";
}

function closebulbicon() {
  document.getElementById("bulbicon").style.display = "none";
}

function calcicon() {
  document.getElementById("calcicon").style.display = "block";
}

function closecalcicon() {
  document.getElementById("calcicon").style.display = "none";
}

function clockicon() {
  document.getElementById("clockicon").style.display = "block";
}

function closeclockicon() {
  document.getElementById("clockicon").style.display = "none";
}

function handbookicon() {
  document.getElementById("handbookicon").style.display = "block";
}

function closehandbookicon() {
  document.getElementById("handbookicon").style.display = "none";
}

function helpicon() {
  document.getElementById("helpicon").style.display = "block";
}

function closehelpicon() {
  document.getElementById("helpicon").style.display = "none";
}

function mindsicon() {
  document.getElementById("mindsicon").style.display = "block";
}

function closemindsicon() {
  document.getElementById("mindsicon").style.display = "none";
}

function refreshicon() {
  document.getElementById("refreshicon").style.display = "block";
}

function closerefreshicon() {
  document.getElementById("refreshicon").style.display = "none";
}

function menuicon() {
  document.getElementById("menuicon").style.display = "block";
}

function closemenuicon() {
  document.getElementById("menuicon").style.display = "none";
}

function terminalicon() {
  document.getElementById("terminalicon").style.display = "block";
}

function closeterminalicon() {
  document.getElementById("terminalicon").style.display = "none";
}

function codeicon() {
  document.getElementById("codeicon").style.display = "block";
}

function closecodeicon() {
  document.getElementById("codeicon").style.display = "none";
}

function installicon() {
  document.getElementById("installicon").style.display = "block";
}

function closeinstallicon() {
  document.getElementById("installicon").style.display = "none";
}

function timeicon() {
  document.getElementById("timeicon").style.display = "block";
}

function closetimeicon() {
  document.getElementById("timeicon").style.display = "none";
}

function gearicon() {
  document.getElementById("gearicon").style.display = "block";
}

function closegearicon() {
  document.getElementById("gearicon").style.display = "none";
}

function messengericon() {
  document.getElementById("messengericon").style.display = "block";
}

function closemessengericon() {
  document.getElementById("messengericon").style.display = "none";
}

function appicon() {
  document.getElementById("appicon").style.display = "block";
}

function closeappicon() {
  document.getElementById("appicon").style.display = "none";
}

function playicon() {
  document.getElementById("playicon").style.display = "block";
}

function closeplayicon() {
  document.getElementById("playicon").style.display = "none";
}

function carticon() {
  document.getElementById("carticon").style.display = "block";
}

function closecarticon() {
  document.getElementById("carticon").style.display = "none";
}

function photoicon() {
  document.getElementById("photoicon").style.display = "block";
}

function closephotoicon() {
  document.getElementById("photoicon").style.display = "none";
}

function hearticon() {
  document.getElementById("hearticon").style.display = "block";
}

function closehearticon() {
  document.getElementById("hearticon").style.display = "none";
}

function foldericon() {
  document.getElementById("foldericon").style.display = "block";
}

function closefoldericon() {
  document.getElementById("foldericon").style.display = "none";
}

function notepadicon() {
  document.getElementById("notepadicon").style.display = "block";
}

function closenotepadicon() {
  document.getElementById("notepadicon").style.display = "none";
}

function gridicon() {
  document.getElementById("gridicon").style.display = "block";
}

function closegridicon() {
  document.getElementById("gridicon").style.display = "none";
}

// light mode //
var element = document.body;

function Lightmode() {
  element.classList.toggle("lightmode");
}

// secure https //

var loc = window.location.href + "";
if (loc.indexOf("http://") == 0) {
  window.location.href = loc.replace("http://", "https://");
}

// Clock js //

function currentTime() {
  var date = new Date(); /* creating object of Date class */
  var hour = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();
  hour = updateTime(hour);
  min = updateTime(min);
  sec = updateTime(sec);
  document.getElementById("clock").innerText =
    hour + " : " + min + " : " + sec; /* adding time to the div */
  var t = setTimeout(function() {
    currentTime();
  }, 1000); /* setting timer */
}

function updateTime(k) {
  if (k < 10) {
    return "0" + k;
  } else {
    return k;
  }
}

currentTime(); /* calling currentTime() function to initiate the process */

//Make the DIV element draggagle:
dragElement(document.getElementById("legendZ"));

function dragElement(elmnt) {
  var pos1 = 0,
    pos2 = 0,
    pos3 = 0,
    pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    /* if present, the header is where you move the DIV from:*/
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    /* otherwise, move the DIV from anywhere inside the DIV:*/
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = elmnt.offsetTop - pos2 + "px";
    elmnt.style.left = elmnt.offsetLeft - pos1 + "px";
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}
